/* libgda-uimm - A C++ wrapper for libgda-ui
 *
 * Copyright (c) 2009, 2010 The libgda-uimm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _LIBGDA_UIMM_H
#define _LIBGDA_UIMM_H

/** @mainpage libgda-uimm Reference Manual
 *
 * @section description Description
 *
 * libgda-uimm is a C++ wrapper for libgda-ui, a library of ui widgets based
 * on GTK+ for <a href="http://www.gnome-db.org/">libgda</a>, the gnome
 * database access library.  libgda-uimm wraps the libgda-ui widgets using
 * <a href="http://www.gtkmm.org">gtkmm</a>.  See, for example, the
 * Gnome::GdaUI::Login widget.
 *
 * @section basics Basic Usage
 *
 * Include the libgda-uimm header:
 * @code
 * #include <libgda-uimm.h>
 * @endcode
 * (You may include individual headers, such as @c libgda-uimm/login.h
 * instead.)
 *
 * If your source file is @c program.cc, you can compile it with:
 * @code
 * g++ program.cc -o program  `pkg-config --cflags --libs libgda-uimm-4.0`
 * @endcode
 *
 * Alternatively, if using autoconf, use the following in @c configure.ac:
 * @code
 * PKG_CHECK_MODULES([LIBGDA_UIMM], [libgda-uimm-4.0])
 * @endcode
 * Then use the generated @c LIBGDA_UIMM_CFLAGS and @c LIBGDA_UIMM_LIBS
 * variables in the
 * project @c Makefile.am files. For example:
 * @code
 * program_CPPFLAGS = $(LIBGDA_UIMM_CFLAGS)
 * program_LDADD = $(LIBGDA_UIMM_LIBS)
 * @endcode
 */

#include <basicform.h>
#include <cloud.h>
#include <combo.h>
#include <dataentry.h>
#include <datafilter.h>
#include <dataproxy.h>
#include <dataproxyinfo.h>
#include <dataselector.h>
#include <datastore.h>
#include <form.h>
#include <grid.h>
#include <login.h>
#include <providerselector.h>
#include <rawform.h>
#include <rawgrid.h>
#include <serveroperation.h>

#endif /* _LIBGDA_UIMM_H */
