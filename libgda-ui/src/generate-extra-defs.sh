#!/bin/bash

# Note that script assumes it resides in the gstreamermm/gstreamer/src
# directory.

DIR=`dirname "$0"`

"$DIR"/../../tools/extra_defs_gen/generate_defs_libgda_ui > "$DIR/libgda_ui_signals.defs"
