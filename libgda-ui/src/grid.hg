/* libgda-uimm - a C++ wrapper for libgda-ui
 *
 * Copyright (c) 2010 libgda-uimm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtkmm/box.h>
#include <libgda-uimm/dataproxy.h>
#include <libgda-uimm/dataselector.h>
#include <libgda-uimm/dataproxyinfo.h>

_DEFS(libgda-uimm,libgda_ui)
_PINCLUDE(gtkmm/private/box_p.h)

namespace Gnome
{

// Gnome::Gda class forward declarations.
namespace Gda
{

class DataModel;

}

namespace GdaUI
{

class RawGrid;

/** Grid - Grid widget to manipulate data in a Gnome::Gda::DataModel, with
 * decorations.
 */
class Grid :
  public Gtk::Box,
  public DataSelector,
  public DataProxy
{
  _CLASS_GTKOBJECT(Grid, GdauiGrid, GDAUI_GRID, Gtk::Box, GtkBox)
  _IMPLEMENTS_INTERFACE(DataSelector)
  _IMPLEMENTS_INTERFACE(DataProxy)

public:
  _WRAP_METHOD_DOCS_ONLY(gdaui_grid_new)
  _WRAP_CTOR(Grid(const Glib::RefPtr<Gnome::Gda::DataModel>& model), gdaui_grid_new)

public:
  _WRAP_METHOD(void set_sample_size(int sample_size), gdaui_grid_set_sample_size)
  _WRAP_PROPERTY("info", DataProxyInfo*)
  _WRAP_PROPERTY("info-flags", DataProxyInfoFlag)
  _WRAP_PROPERTY("model", Glib::RefPtr<Gnome::Gda::DataModel>)
  _WRAP_PROPERTY("raw-grid", RawGrid*)
};

} // namespace GdaUI

} // namespace Gnome
