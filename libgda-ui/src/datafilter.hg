/* libgda-uimm - a C++ wrapper for libgda-ui
 *
 * Copyright (c) 2010 libgda-uimm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtkmm/box.h>

_DEFS(libgda-uimm,libgda_ui)
_PINCLUDE(gtkmm/private/box_p.h)

namespace Gnome
{

// Gnome::Gda class forward declarations.
namespace Gda
{

}

namespace GdaUI
{

class DataProxy;

/** DataFilter - Entrer rules to filter the rows in a DataProxy.  The
 * DataFilter widget can be used as a standalone widget, but is also used
 * internally by the DataProxyInfo widget for its search option.
 */
class DataFilter :
  public Gtk::Box
{
  _CLASS_GTKOBJECT(DataFilter, GdauiDataFilter, GDAUI_DATA_FILTER, Gtk::Box, GtkBox)

public:
  _WRAP_METHOD_DOCS_ONLY(gdaui_data_filter_new)
  _WRAP_CTOR(DataFilter(const Glib::RefPtr<DataProxy>& data_widget), gdaui_data_filter_new)

public:
  _WRAP_PROPERTY("data-widget", Glib::RefPtr<DataProxy>)
};

} // namespace GdaUI

} // namespace Gnome
