/* libgda-uimm - a C++ wrapper for libgda-ui
 *
 * Copyright (c) 2010 libgda-uimm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgda-ui/gdaui-data-entry.h>
#include <glibmm/interface.h>
#include <libgdamm/datamodel.h>

_DEFS(libgda-uimm,libgda_ui)

namespace Gnome
{

// Gnome::Gda class forward declarations.
namespace Gda
{

class DataHandler;

}

namespace GdaUI
{

/** DataEntry - Data entry widget.
 * The DataEntry is an interface for widgets (simple or complex) which lets
 * the user view and/or modify a GValue.
 *
* This interface is implemented by widgets which feature data editing (usually
* composed of an editing area and a button to have some more control on the
* value being edited). The interface allows to control how the widget works
* and to query the value and the attributes of the data held by the widget.
 *
* The widget can store the original value (to be able to tell if the value has
* been changed by the user) and a default value (which will be returned if the
* user explicitly forces the widget to be set to the default value). Control
* methods allow to set the type of value to be edited (the requested type must
* be compatible with what the widget can handle), set the value (which
* replaces the currently edited value), set the value and the original value
* (the value passed as argument is set and is also considered to be the
* original value).
 *
* DataEntry widgets are normally created using the create() method.
 */

class DataEntry :
  public Glib::Interface
{
  _CLASS_INTERFACE(DataEntry, GdauiDataEntry, GDAUI_DATA_ENTRY, GdauiDataEntryIface)

public:
  _WRAP_METHOD(static Glib::RefPtr<DataEntry> create(GType type, const Glib::ustring& plugin_name = Glib::ustring()), gdaui_new_data_entry)

  _WRAP_METHOD(void set_value_type(GType type), gdaui_data_entry_set_value_type)
  _WRAP_METHOD(GType get_value_type() const, gdaui_data_entry_get_value_type)
  _WRAP_METHOD(void set_value(const Glib::ValueBase& value), gdaui_data_entry_set_value)

  /** Push a value into the DataEntry. The value parameter must be of the type
   * specified using set_value_type().
   *
   * @param value The value to set.
   *
   * @newin{4,2}
   */
  template <class DataType>
  void set_value(const DataType& value);

  /** Fetch the value held in the DataEntry widget. If the value is set to
   * NULL, the returned value is of type GDA_TYPE_NULL. If the value is set to
   * default, then the returned value is of type GDA_TYPE_NULL or is the
   * default value if it has been provided to the widget (and is of the same
   * type as the one provided by the data entry).
   *
   * @param value A place in which to store the value.  The value must be
   * uninitialized.
   *
   * @newin{4,2}
   */
  void get_value(Glib::ValueBase& value) const;
  _IGNORE(gdaui_data_entry_get_value)

  /** Fetch the value held in the DataEntry widget. If the value is set to
   * NULL, the returned value is of type GDA_TYPE_NULL. If the value is set to
   * default, then the returned value is of type GDA_TYPE_NULL or is the
   * default value if it has been provided to the widget (and is of the same
   * type as the one provided by the data entry).
   *
   * @param value A place in which to store the value.  The value must be
   * uninitialized.
   *
   * @newin{4,2}
   */
  template<class DataType>
  void get_value(DataType& value) const;

  _WRAP_METHOD(bool content_is_valid(), gdaui_data_entry_content_is_valid, errthrow)

  _WRAP_METHOD(void set_reference_value(const Glib::ValueBase& value), gdaui_data_entry_set_reference_value)

  /** Push a value into the DataEntry in the same way as set_value() but
   * also sets this value as the reference value.
   *
   * @param value The value to set.
   *
   * @newin{4,2}
   */
  template <class DataType>
  void set_reference_value(const DataType& value);
  
  /** Fetch the reference value held in the GdauiDataEntry widget.
   *
   * @param value A place in which to store the value.  The value must be
   * uninitialized and should not be modified.
   *
   * @newin{4,2}
   */
  void get_reference_value(Glib::ValueBase& value) const;
  _IGNORE(gdaui_data_entry_get_reference_value)
  
  _WRAP_METHOD(void set_reference_current(), gdaui_data_entry_set_reference_current)

  /** Fetch the reference value held in the GdauiDataEntry widget.
   *
   * @param value A place in which to store the value.  The value must be
   * uninitialized and should not be modified.
   *
   * @newin{4,2}
   */
  template<class DataType>
  void get_reference_value(DataType& value) const;

  _WRAP_METHOD(void set_default_value(const Glib::ValueBase& value), gdaui_data_entry_set_default_value)

  /** Sets the default value for the DataEntry which gets displayed when the
   * user forces the default value. If it is not set then it is set to type
   * GDA_TYPE_NULL.  The value parameter must be of the type specified using
   * set_value_type().
   *
   * @param value The value to set.
   *
   * @newin{4,2}
   */
  template <class DataType>
  void set_default_value(const DataType& value);

  _WRAP_METHOD(void set_attributes(Gnome::Gda::ValueAttribute attrs, Gnome::Gda::ValueAttribute mask), gdaui_data_entry_set_attributes)
  _WRAP_METHOD(Gnome::Gda::ValueAttribute get_attributes() const, gdaui_data_entry_get_attributes)
  _WRAP_METHOD(Glib::RefPtr<Gnome::Gda::DataHandler> get_handler(), gdaui_data_entry_get_handler)
  _WRAP_METHOD(Glib::RefPtr<const Gnome::Gda::DataHandler> get_handler() const, gdaui_data_entry_get_handler, constversion)
  _WRAP_METHOD(bool can_expand(bool horiz) const, gdaui_data_entry_can_expand)
  _WRAP_METHOD(void set_editable(bool editable), gdaui_data_entry_set_editable)
  _WRAP_METHOD(bool get_editable() const, gdaui_data_entry_get_editable)
  _WRAP_METHOD(void grab_focus(), gdaui_data_entry_grab_focus)

  _WRAP_SIGNAL(void contents_activated(), "contents-activated")
  _WRAP_SIGNAL(void contents_modified(), "contents-modified")

  //TODO: The class declaration seems to suggest that this signal throws a
  //GError.
  //_WRAP_SIGNAL(void contents_valid(), "contents-valid", errthrow)

  _WRAP_SIGNAL(void expand_changed(), "expand-changed")
  _WRAP_SIGNAL(void status_changed(), "status-changed")

  _WRAP_VFUNC(void set_value_type(GType type), "set_value_type")
  _WRAP_VFUNC(GType get_value_type() const, "get_value_type")
  _WRAP_VFUNC(void set_value(const Glib::ValueBase& value), "set_value")

  void get_value_vfunc(Glib::ValueBase& value) const;

  _WRAP_VFUNC(void set_ref_value(const Glib::ValueBase& value), "set_ref_value")


  void get_ref_value_vfunc(Glib::ValueBase& value) const;

  _WRAP_VFUNC(void set_value_default(const Glib::ValueBase& value), "set_value_default")
  _WRAP_VFUNC(void set_attributes(Gnome::Gda::ValueAttribute attrs, Gnome::Gda::ValueAttribute mask), "set_attributes")
  _WRAP_VFUNC(Gnome::Gda::ValueAttribute get_attributes() const, "get_attributes")

#m4 _CONVERSION(`GdaDataHandler', `Glib::RefPtr<Gnome::Gda::DataHandler>', `Glib::wrap($3, true)')
  _WRAP_VFUNC(Glib::RefPtr<Gnome::Gda::DataHandler> get_handler() const, "get_handler")

  _WRAP_VFUNC(bool can_expand(bool horiz) const, "can-expand")
  _WRAP_VFUNC(void set_editable(bool editable), "set_editable")
  _WRAP_VFUNC(void grab_focus(), "grab-focus")

protected:
#m4begin
  _PUSH(SECTION_PCC_CLASS_INIT_VFUNCS)
  klass->get_value = &get_value_vfunc_callback;
  klass->get_ref_value = &get_ref_value_vfunc_callback;
  _SECTION(SECTION_PH_VFUNCS)
  static GValue* get_value_vfunc_callback(GdauiDataEntry* self);
  static const GValue* get_ref_value_vfunc_callback(GdauiDataEntry* self);
  _POP()
#m4end
};

/***************************** DataEntry *****************************/

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template <class DataType>
void DataEntry::set_value(const DataType& value)
{
  typedef Glib::Value<DataType> ValueType;

  // Allow a null value.
  if(value == 0)
  {
    gdaui_data_entry_set_value(gobj(), 0);
    return;
  }

  ValueType temp;
  temp.init(ValueType::value_type());
  temp.set(value);
  this->set_value(temp);
}

template<class DataType>
void DataEntry::get_value(DataType& value) const
{
  Glib::Value<DataType> temp;
  this->get_value(temp);
  value = temp.get();
}

template<class DataType>
void DataEntry::get_reference_value(DataType& value) const
{
  Glib::Value<DataType> temp;
  this->get_reference_value(temp);
  value = temp.get();
}

template <class DataType>
void DataEntry::set_default_value(const DataType& value)
{
  typedef Glib::Value<DataType> ValueType;

  // Allow a null value.
  if(value == 0)
  {
    gdaui_data_entry_set_default_value(gobj(), 0);
    return;
  }

  ValueType temp;
  temp.init(ValueType::value_type());
  temp.set(value);
  this->set_default_value(temp);
}

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

} // namespace GdaUI

} // namespace Gnome
