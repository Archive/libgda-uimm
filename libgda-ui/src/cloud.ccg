/* libgda-uimm - a C++ wrapper for libgda-ui
 * 
 * Copyright (c) 2010 libgda-uimm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgda-ui/gdaui-cloud.h>
#include <libgdamm/datamodel.h>

namespace
{

extern "C"
{

static gdouble Cloud_Weight_libgda_uimm_callback(GdaDataModel *model,
  gint row, gpointer data)
{
  Gnome::GdaUI::Cloud::SlotWeight* the_slot =
    static_cast<Gnome::GdaUI::Cloud::SlotWeight*>(data);

  try
  {
    return (*the_slot)(Glib::wrap(model, true), row);
  }
  catch(...)
  {
    Glib::exception_handlers_invoke();
  }

  return 0;
}

} // extern "C"

} // anonymous namespace

namespace Gnome
{

namespace GdaUI
{

void Cloud::set_weight_slot(const SlotWeight& slot)
{
  // Create a copy of the slot.  A pointer to this copy will be passed through
  // the call back's data parameter.  It will be destroyed with the
  // std::auto_ptr<>.
  m_slot.reset(new SlotWeight(slot));

  gdaui_cloud_set_weight_func(gobj(), &Cloud_Weight_libgda_uimm_callback,
    m_slot.get());
}

void Cloud::unset_weight_slot()
{
  // The docs say to use NULL for the callback which will remove the weight
  // callback.
  gdaui_cloud_set_weight_func(gobj(), 0, 0);
}

} /* namespace GdaUI */

} /* namespace Gnome */
