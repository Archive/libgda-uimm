/* libgda-uimm - a C++ wrapper for libgda-ui
 *
 * Copyright (c) 2010 libgda-uimm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgda/gda-config.h>
#include <libgda-ui/gdaui-login.h>
#include <gtkmm/box.h>

_DEFS(libgda-uimm,libgda_ui)
_PINCLUDE(gtkmm/private/box_p.h)

namespace Gnome
{

// Gnome::Gda class forward declartions.
namespace Gda
{

}

namespace GdaUI
{

_WRAP_ENUM(LoginMode, GdauiLoginMode, s#^UI_##)

/** Login - Connection opening dialog.
 * The Login widget can be used when the user needs to enter data to open a
 * connection. It can be customized in several ways:
 *
 *   - Data source (DSN) selection can be shown or hidden.
 *   - The button to launch the control center to declare new data sources can
 *   be shown or hidden.
 *   - The form to open a connection not using a DSN can be shown or hidden.
 */
class Login :
  public Gtk::Box
{
  _CLASS_GTKOBJECT(Login, GdauiLogin, GDAUI_LOGIN, Gtk::Box, GtkBox)

public:
  _WRAP_METHOD_DOCS_ONLY(gdaui_login_new)
  _WRAP_CTOR(Login(const Glib::ustring& dsn), gdaui_login_new)

public:
  _WRAP_METHOD(void set_mode(LoginMode mode), gdaui_login_set_mode)
  _WRAP_METHOD(const GdaDsnInfo* get_connection_information(), gdaui_login_get_connection_information)
  _WRAP_METHOD(void set_dsn(const Glib::ustring& dsn), gdaui_login_set_dsn)
  _WRAP_METHOD(void set_connection_information(const GdaDsnInfo *cinfo), gdaui_login_set_connection_information)

  _WRAP_SIGNAL(void changed(bool is_valid), "changed")

  _WRAP_PROPERTY("dsn", Glib::ustring)
  _WRAP_PROPERTY("mode", LoginMode)
  _WRAP_PROPERTY("valid", bool)
};

} // namespace GdaUI

} // namespace Gnome
