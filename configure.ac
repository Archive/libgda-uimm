## Copyright (c) 2009 The libgda-uimm Development Team
##
## This libray is free software: you can redistribute it and/or modify it
## under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 2.1 of the License, or (at
## your option) any later version.
##
## This library is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
## or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
## License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this library.  If not, see <http://www.gnu.org/licenses/>.

AC_INIT([libgda-uimm], [4.99.4], [gtkmm-list@gnome.org], [libgda-uimm], [http://www.gtkmm.org/])
AC_PREREQ([2.59])

AC_CONFIG_SRCDIR([libgda-ui/libgda-uimm.h])
AC_CONFIG_AUX_DIR([build])
AC_CONFIG_MACRO_DIR([build])
AC_CONFIG_HEADERS([config.h libgda-ui/libgda-uimmconfig.h])

MM_PREREQ([0.9.5])
MM_INIT_MODULE([libgda-uimm-5.0])

# Copy the mm-common .pl scripts into docs/,
# and use them from there,
# so we can dist them to avoid a tarball-build dependency.
MM_CONFIG_DOCTOOL_DIR([docs])

# http://www.gnu.org/software/libtool/manual/html_node/Updating-version-info.html
AC_SUBST([LIBGDA_UIMM_SO_VERSION], [0:0:0])

AM_INIT_AUTOMAKE([1.9 -Wno-portability dist-bzip2 no-define nostdinc tar-ustar])
AM_MAINTAINER_MODE
AC_ARG_VAR([ACLOCAL_FLAGS], [aclocal flags, e.g. -I <macro dir>])

AC_PROG_CXX
AC_DISABLE_STATIC
AC_LIBTOOL_WIN32_DLL
AC_PROG_LIBTOOL

AC_SUBST([LIBGDA_UIMM_MODULES], ['libgda-5.0 >= 4.9.4 libgda-ui-5.0 >= 4.9.4 libgdamm-5.0 >= 4.99.4 gtkmm-3.0 >= 3.0.0'])
PKG_CHECK_MODULES([LIBGDA_UIMM], [$LIBGDA_UIMM_MODULES])

MM_PKG_CONFIG_SUBST([GTHREAD_CFLAGS], [--cflags-only-other gthread-2.0])
MM_PKG_CONFIG_SUBST([GMMPROC_DIR], [--variable=gmmprocdir glibmm-2.4])
MM_PKG_CONFIG_SUBST([GMMPROC_EXTRA_M4_DIR], [--variable=gmmprocm4dir gtkmm-3.0 pangomm-1.4 atkmm-1.6])

MM_ARG_DISABLE_DEPRECATED_API
MM_ARG_ENABLE_DOCUMENTATION
MM_ARG_WITH_TAGFILE_DOC([libstdc++.tag], [mm-common-libstdc++])
MM_ARG_WITH_TAGFILE_DOC([libsigc++-2.0.tag], [sigc++-2.0])
MM_ARG_WITH_TAGFILE_DOC([glibmm-2.4.tag], [glibmm-2.4])
MM_ARG_WITH_TAGFILE_DOC([gtkmm-2.4.tag], [gtkmm-2.4])
MM_ARG_WITH_TAGFILE_DOC([libgdamm-4.0.tag], [libgdamm-4.0])

AC_LANG([C++])
MM_ARG_ENABLE_WARNINGS([LIBGDA_UIMM_WXXFLAGS],
                       [-Wall],
                       [-pedantic -Wall -Wextra],
                       [G LIBGDA_UI])

AC_CONFIG_FILES([Makefile
                 tools/Makefile
                 libgda-ui/${LIBGDA_UIMM_MODULE_NAME}.pc:libgda-ui/libgda-uimm.pc.in
                 libgda-ui/${LIBGDA_UIMM_MODULE_NAME}-uninstalled.pc:libgda-ui/libgda-uimm-uninstalled.pc.in
                 libgda-ui/src/Makefile
                 libgda-ui/libgda-uimm/Makefile
                 docs/Makefile
                 docs/reference/Doxyfile])
AC_OUTPUT
