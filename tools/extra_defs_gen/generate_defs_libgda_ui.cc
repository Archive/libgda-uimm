/* generate_defs_libgda_ui.cc
 *
 * Copyright (c) 2009 The libgda-uimm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glibmm_generate_extra_defs/generate_extra_defs.h>
#include <libgda/gda-data-model.h>
#include <libgda-ui/gdaui-cloud.h>
#include <libgda-ui/gdaui-combo.h>
#include <libgda-ui/gdaui-combo.h>
#include <libgda-ui/gdaui-data-entry.h>
#include <libgda-ui/gdaui-data-filter.h>
#include <libgda-ui/gdaui-data-proxy.h>
#include <libgda-ui/gdaui-data-proxy-info.h>
#include <libgda-ui/gdaui-data-selector.h>
#include <libgda-ui/gdaui-data-selector.h>
#include <libgda-ui/gdaui-data-store.h>
#include <libgda-ui/gdaui-form.h>
#include <libgda-ui/gdaui-grid.h>
#include <libgda-ui/gdaui-login.h>
#include <libgda-ui/gdaui-provider-selector.h>
#include <libgda-ui/gdaui-raw-form.h>
#include <libgda-ui/gdaui-raw-grid.h>
#include <libgda-ui/gdaui-rt-editor.h>
#include <libgda-ui/gdaui-server-operation.h>
#include <libgda-ui/gdaui-tree-store.h>
#include <iostream>

int main(int, char**)
{
  // g_type_init() is deprecated as of glib-2.36.
  // g_type_init();

  std::cout << get_defs(GDAUI_TYPE_BASIC_FORM)          << std::endl
            << get_defs(GDAUI_TYPE_CLOUD)               << std::endl
            << get_defs(GDAUI_TYPE_COMBO)               << std::endl
            << get_defs(GDAUI_TYPE_COMBO)               << std::endl
            << get_defs(GDAUI_TYPE_DATA_ENTRY)          << std::endl
            << get_defs(GDAUI_TYPE_DATA_FILTER)         << std::endl
            << get_defs(GDAUI_TYPE_DATA_PROXY_INFO)     << std::endl
            << get_defs(GDAUI_TYPE_DATA_PROXY)          << std::endl
            << get_defs(GDAUI_TYPE_DATA_SELECTOR)       << std::endl
            << get_defs(GDAUI_TYPE_DATA_SELECTOR)       << std::endl
            << get_defs(GDAUI_TYPE_DATA_STORE)          << std::endl
            << get_defs(GDAUI_TYPE_FORM)                << std::endl
            << get_defs(GDAUI_TYPE_GRID)                << std::endl
            << get_defs(GDAUI_TYPE_LOGIN)               << std::endl
            << get_defs(GDAUI_TYPE_PROVIDER_SELECTOR)   << std::endl
            << get_defs(GDAUI_TYPE_RAW_FORM)            << std::endl
            << get_defs(GDAUI_TYPE_RAW_GRID)            << std::endl
            << get_defs(GDAUI_TYPE_RT_EDITOR)           << std::endl
            << get_defs(GDAUI_TYPE_SERVER_OPERATION)    << std::endl
            << get_defs(GDAUI_TYPE_TREE_STORE)          << std::endl
            ;
  return 0;
}
