dnl Copyright (c) 2009 The libgda-uimm Development Team

dnl Enums:
_CONV_ENUM(Gdaui,Action)
_CONV_ENUM(Gdaui,BasicFormPart)
_CONV_ENUM(Gdaui,DataProxyInfoFlag)
_CONV_ENUM(Gdaui,DataProxyWriteMode)
_CONV_ENUM(Gdaui,LoginMode)

dnl ActionGroup:
_CONVERSION(`GtkActionGroup*', `Glib::RefPtr<Gtk::ActionGroup>', `Glib::wrap($3)')
_CONVERSION(`Glib::RefPtr<Gtk::ActionGroup>', `GtkActionGroup*', `Glib::unwrap($3)')

dnl DataEntry:
_CONVERSION(`GdauiDataEntry*', `Glib::RefPtr<DataEntry>', `Glib::wrap($3)')

dnl Gnome::Gda::DataHandler:
_CONVERSION(`GdaDataHandler*', `Glib::RefPtr<Gnome::Gda::DataHandler>', `Glib::wrap($3)')
_CONVERSION(`Glib::RefPtr<Gnome::Gda::DataHandler>', `GdaDataHandler*', `Glib::unwrap($3)')

dnl Gnome::Gda::DataProxy:
_CONVERSION(`GdaDataProxy*', `Glib::RefPtr<Gnome::Gda::DataProxy>', `Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<Gnome::Gda::DataProxy>&', `GdaDataProxy*', `Glib::unwrap($3)')
_CONVERSION(`Glib::RefPtr<Gnome::Gda::DataProxy>', `GdaDataProxy*', `Glib::unwrap($3)')

dnl Gnome::Gdaui::DataProxy:
_CONVERSION(`const Glib::RefPtr<DataProxy>&', `GdauiDataProxy*', `Glib::unwrap($3)')
_CONVERSION(`GdauiDataProxy*', `Glib::RefPtr<DataProxy>', `Glib::wrap($3)')

dnl DataModel:
_CONVERSION(`const Glib::RefPtr<Gnome::Gda::DataModel>&', `GdaDataModel*', `Glib::unwrap($3)')
_CONVERSION(`GdaDataModel*', `Glib::RefPtr<Gnome::Gda::DataModel>', `Glib::wrap($3)')
_CONVERSION(`Glib::RefPtr<Gnome::Gda::DataModel>', `GdaDataModel*', `Glib::unwrap($3)')

dnl DataModelIter:
_CONVERSION(`GdaDataModelIter*', `Glib::RefPtr<Gnome::Gda::DataModelIter>', `Glib::wrap($3)')
_CONVERSION(`Glib::RefPtr<Gnome::Gda::DataModelIter>', `GdaDataModelIter*', `Glib::unwrap($3)')

dnl Holder:
_CONVERSION(`const Glib::RefPtr<Gnome::Gda::Holder>&', `GdaHolder*', `Glib::unwrap($3)')

dnl Menu:
_CONVERSION(`GtkMenu*', `Gtk::Menu*', `Glib::wrap($3)')
_CONVERSION(`Gtk::Menu*', `GtkMenu*', `Glib::unwrap($3)')

dnl ServerOperation:
_CONVERSION(`const Glib::RefPtr<Gnome::Gda::ServerOperation>&', `GdaServerOperation*', `Glib::unwrap($3)')

dnl ServerProvider:
_CONVERSION(`GdaServerProvider*', `Glib::RefPtr<Gnome::Gda::ServerProvider>', `Glib::wrap($3)')

dnl Set:
_CONVERSION(`const Glib::RefPtr<Gnome::Gda::Set>&', `GdaSet*', `Glib::unwrap($3)')
_CONVERSION(`GdaSet*', `Glib::RefPtr<Gnome::Gda::Set>', `Glib::wrap($3)')

dnl SizeGroup:
_CONVERSION(`const Glib::RefPtr<Gtk::SizeGroup>&', `GtkSizeGroup*', `Glib::unwrap($3)')

dnl Gtk::TreeModel::iterator:
_CONVERSION(`const Gtk::TreeModel::const_iterator&', `GtkTreeIter*', `const_cast<GtkTreeIter*>($3->gobj())')
_CONVERSION(`const Gtk::TreeModel::iterator&', `GtkTreeIter*', `const_cast<GtkTreeIter*>($3->gobj())')
_CONVERSION(`Gtk::TreeModel::iterator&', `GtkTreeIter*', `const_cast<GtkTreeIter*>($3->gobj())')

dnl Window:
_CONVERSION(`Gtk::Window*', `GtkWindow*', `Glib::unwrap($3)')

dnl Gnome::Gda::ValueAttribute:
_CONVERSION(`Gnome::Gda::ValueAttribute', `GdaValueAttribute', `$3')
_CONVERSION(`GdaValueAttribute', `Gnome::Gda::ValueAttribute', `$3')

dnl General conversions:
