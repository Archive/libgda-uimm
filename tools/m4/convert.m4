## Copyright (c) 2009 The libgda-uimm Development Team

# Other libraries, such as libgnomeuimm, can provide their own convert.m4 files,
# Maybe choosing to include the same files as this one.

include(convert_gtkmm.m4)
include(convert_libgda_uimm.m4)
